import re

import datetime
from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _
from rest_framework.exceptions import ValidationError as RestValidationError


def validate_manager_and_driver_not_equal_validator(value1, value2):
    if value1 == value2:
        raise ValidationError('Driver search and Manager search can not be same!')


def validate_manager_and_driver_have_same_time_interval_validator(value1, value2):
    if value1.date_from > value2.date_from:
        raise ValidationError('Driver search and Manager search does not match by time')


def validate_gsm_or_email(email, gsm):
    if email or gsm:
        pass
    else:
        raise RestValidationError("email or gsm field must be filled!")


def validate_phone_number(value):
    if not re.search(r'\d{10}', value):
        raise ValidationError(
            _('%(value)s is not a valid phone number'),
            params={'value': value},
        )


def validate_date_is_not_past(value):
    if value:
        if value < datetime.date.today():
            raise ValidationError('date must be greater than now')
