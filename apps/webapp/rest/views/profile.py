from django_filters import rest_framework as df_filters
from rest_framework import viewsets, filters
from rest_framework.permissions import IsAuthenticated

from apps.webapp.models import Profile
from apps.webapp.rest.filters import ProfileFilter
from apps.webapp.rest.serializers import ProfileSerializer, ReadOnlyProfileSerializer


class ProfileView(viewsets.ModelViewSet):
    """

    """
    filter_class = ProfileFilter
    search_fields = ('username', 'rating')
    filter_backends = (df_filters.DjangoFilterBackend, filters.SearchFilter, filters.OrderingFilter)
    ordering_fields = '__all__'
    ordering = ('updated_at',)  # default ordering
    permission_classes = (IsAuthenticated, )

    def get_queryset(self):
        qry = Profile.objects.all()
        return qry

    def get_serializer_class(self):
        if self.action in ["create", "update"]:
            return ProfileSerializer
        return ReadOnlyProfileSerializer
