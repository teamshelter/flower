from django_filters import rest_framework as df_filters
from rest_framework import viewsets, filters
from rest_framework.permissions import IsAuthenticated

from apps.webapp.models import Match
from apps.webapp.rest.filters import MatchFilter
from apps.webapp.rest.serializers import MatchSerializer, MatchUpdateSerializer, ReadOnlyMatchSerializer


class MatchView(viewsets.ModelViewSet):
    """

    """
    filter_class = MatchFilter
    search_fields = ('details', 'is_approved', 'updated_by', 'manager_search__price', 'driver_search__price')
    filter_backends = (df_filters.DjangoFilterBackend, filters.SearchFilter, filters.OrderingFilter)
    ordering_fields = '__all__'
    ordering = ('-updated_at',)  # default ordering
    permission_classes = (IsAuthenticated, )

    def get_queryset(self):
        qry = Match.objects.all()
        return qry

    def get_serializer_class(self):
        if self.action in ["create", ]:
            return MatchSerializer
        elif self.action in ["update", ]:
            return MatchUpdateSerializer
        return ReadOnlyMatchSerializer
