from django_filters import rest_framework as df_filters
from rest_framework import viewsets, filters

from apps.webapp.models import JobSearch
from apps.webapp.rest.filters import JobSearchFilter
from apps.webapp.rest.serializers import JobSearchSerializer, ReadOnlyJobSearchSerializer


class JobSearchView(viewsets.ModelViewSet):
    """

    """
    filter_class = JobSearchFilter
    search_fields = (
        'quota', 'exchange_location',
        'primary_gsm', 'secondary_gsm', 'fullname', 'age',
        'is_retired', 'card_no', 'past_taxi_stop_list',
        'reference', 'year_in_job', 'current_employment',
        'is_married', 'blood_group', 'year_in_istanbul'
    )
    filter_backends = (df_filters.DjangoFilterBackend, filters.SearchFilter, filters.OrderingFilter)
    ordering_fields = '__all__'
    ordering = ('-updated_at',)  # default ordering

    def get_queryset(self):
        qry = JobSearch.objects.all()
        return qry

    def get_serializer_class(self, *args, **kwargs):
        if self.action in ['create', 'update', ]:
            return JobSearchSerializer
        return ReadOnlyJobSearchSerializer
