from django.contrib.auth import get_user_model
from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated

from apps.webapp.rest.serializers import UserSerializer, ReadOnlyUserSerializer, UserUpdateSerializer

User = get_user_model()


class UserView(viewsets.ModelViewSet):
    queryset = User.objects.all()
    permission_classes = (IsAuthenticated, )

    def get_queryset(self):
        return User.objects.filter(id=self.request.user.id)

    def get_serializer_class(self):
        if self.action in ['create', ]:
            return UserSerializer
        elif self.action in ['update', ]:
            return UserUpdateSerializer
        return ReadOnlyUserSerializer
