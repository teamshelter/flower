from django_filters import rest_framework as df_filters
from rest_framework import viewsets, filters

from apps.webapp.models import DriverSearch
from apps.webapp.rest.filters import DriverSearchFilter
from apps.webapp.rest.serializers import DriverSearchSerializer, ReadOnlyDriverSearchSerializer


class DriverSearchView(viewsets.ModelViewSet):
    """

    """
    filter_class = DriverSearchFilter
    search_fields = (
        'quota', 'exchange_location',
        'primary_gsm', 'secondary_gsm', 'fullname', 'age',
        'is_retired', 'period', 'duration', 'requested_fee'
    )
    filter_backends = (df_filters.DjangoFilterBackend, filters.SearchFilter, filters.OrderingFilter)
    ordering_fields = '__all__'
    ordering = ('-updated_at',)  # default ordering

    def get_queryset(self):
        qry = DriverSearch.objects.all()
        return qry

    def get_serializer_class(self, *args, **kwargs):
        if self.action in ['create', 'update', ]:
            return DriverSearchSerializer
        return ReadOnlyDriverSearchSerializer
