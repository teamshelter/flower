from rest_framework import serializers

from apps.webapp.models import DriverSearch


class DriverSearchSerializer(serializers.ModelSerializer):
    """

    """
    class Meta:
        model = DriverSearch
        fields = (
            'id',
            'quota',
            'exchange_location',
            'primary_gsm',
            'secondary_gsm',
            'fullname',
            'age',
            'is_retired',
            'period',
            'duration',
            'requested_fee'
        )


class ReadOnlyDriverSearchSerializer(serializers.ModelSerializer):
    """

    """
    class Meta:
        model = DriverSearch
        fields = (
            'id',
            'created_at',
            'updated_at',
            'quota',
            'exchange_location',
            'primary_gsm',
            'secondary_gsm',
            'fullname',
            'age',
            'is_retired',
            'period',
            'duration',
            'requested_fee',
            'representation'
        )


class DriverSearchUpdateSerializer(serializers.ModelSerializer):
    """

    """
    class Meta:
        model = DriverSearch
        fields = (
            'comments'
        )
