from rest_framework import serializers

from apps.webapp.models import Profile


class ReadOnlyProfileSerializer(serializers.ModelSerializer):
    """

    """

    class Meta:
        model = Profile
        fields = (
            'id',
            'username',
            'rating',
            'updated_at',
            'created_at'
        )


class ProfileSerializer(serializers.ModelSerializer):
    """

    """

    class Meta:
        model = Profile
        fields = (
            'username',
        )
