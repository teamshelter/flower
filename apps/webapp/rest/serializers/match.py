from rest_framework import serializers

from apps.webapp.models import Match
from apps.webapp.rest.serializers import ReadOnlyJobSearchSerializer, ReadOnlyDriverSearchSerializer


class ReadOnlyMatchSerializer(serializers.ModelSerializer):
    """

    """
    driver_search = ReadOnlyDriverSearchSerializer(read_only=True)
    job_search = ReadOnlyJobSearchSerializer(read_only=True)
    details = serializers.ReadOnlyField()

    class Meta:
        model = Match
        fields = (
            'id',
            'job_search',
            'driver_search',
            'details',
            'rating',
            'updated_at',
            'created_at'
        )


class MatchSerializer(serializers.ModelSerializer):
    """

    """

    class Meta:
        model = Match
        fields = (
            'job_search',
            'driver_search',
            'details',
        )


class MatchUpdateSerializer(serializers.ModelSerializer):
    """

    """
    class Meta:
        model = Match
        fields = (
            'rating',
            'details'
        )
