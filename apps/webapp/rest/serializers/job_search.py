from rest_framework import serializers

from apps.webapp.models import JobSearch


class JobSearchSerializer(serializers.ModelSerializer):
    """

    """
    class Meta:
        model = JobSearch
        fields = (
            'id',
            'quota',
            'exchange_location',
            'primary_gsm',
            'secondary_gsm',
            'fullname',
            'age',
            'is_retired',
            'card_no',
            'past_taxi_stop_list',
            'reference',
            'year_in_job',
            'current_employment',
            'is_married',
            'blood_group',
            'year_in_istanbul'
        )


class ReadOnlyJobSearchSerializer(serializers.ModelSerializer):
    """

    """
    class Meta:
        model = JobSearch
        fields = (
            'id',
            'created_at',
            'updated_at',
            'quota',
            'exchange_location',
            'primary_gsm',
            'secondary_gsm',
            'fullname',
            'age',
            'is_retired',
            'card_no',
            'past_taxi_stop_list',
            'reference',
            'year_in_job',
            'current_employment',
            'is_married',
            'blood_group',
            'year_in_istanbul',
            'representation'
        )


class JobSearchUpdateSerializer(serializers.ModelSerializer):
    """

    """
    class Meta:
        model = JobSearch
        fields = (
            'comments'
        )
