import django_filters

from apps.webapp.models import JobSearch, DriverSearch


class JobSearchFilter(django_filters.FilterSet):
    """

    """
    class Meta:
        model = JobSearch
        # use url filters as follows
        """
        to filter against date --- api/path/?updated_at__gte=2018-03-03
        """
        fields = {
            'updated_at': [
                'lte', 'gte', 'exact'
            ]
        }


class DriverSearchFilter(django_filters.FilterSet):
    """

    """
    class Meta:
        model = DriverSearch
        # use url filters as follows
        """
        to filter against date --- api/path/?updated_at__gte=2018-03-03
        """
        fields = {
            'updated_at': [
                'lte', 'gte', 'exact'
            ]
        }
