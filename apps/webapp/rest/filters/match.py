import django_filters

from apps.webapp.models import Match


class MatchFilter(django_filters.FilterSet):
    """

    """
    class Meta:
        model = Match
        # use url filters as follows
        """
        to filter against date --- api/path/?updated_at__gte=2018-03-03
        to filter against year --- api/path/?updated_at__year__gte=2019
        to filter against month --- api/path/?updated_at__month__lt=9
        """
        fields = {
            'updated_at': [
                'lte', 'gte'
            ],
            'created_at': [
                'lte', 'gte'
            ],
        }

