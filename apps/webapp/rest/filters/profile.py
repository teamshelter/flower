import django_filters

from apps.webapp.models import Profile


class ProfileFilter(django_filters.FilterSet):
    """

    """
    class Meta:
        model = Profile
        fields = {
            'username': [
                'exact',
            ],
        }

