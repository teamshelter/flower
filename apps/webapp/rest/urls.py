from django.conf.urls import url
from django.urls import include, path
from rest_framework.routers import DefaultRouter

from apps.webapp.rest.views.match import MatchView
from apps.webapp.rest.views.job_search import JobSearchView
from apps.webapp.rest.views.driver_search import DriverSearchView
from apps.webapp.rest.views.user import UserView

router = DefaultRouter()
router.register(r'contract', MatchView, base_name="contract")
router.register(r'job_searches', JobSearchView, base_name='job_search')
router.register(r'driver_searches', DriverSearchView, base_name='driver_search')
router.register(r'user', UserView)


urlpatterns = [
    url(r'', include(router.urls)),
    path('auth/', include('rest_auth.urls')),
    # path('auth/registration', include('rest_auth.registration.urls')),
    # path('accounts/', include('allauth.urls')),
]
