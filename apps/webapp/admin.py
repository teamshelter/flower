import csv

from django.contrib import admin
from django.contrib.auth import get_user_model
from django.db.models import Count, Q
from django.http import HttpResponse

from apps.webapp.models import Match, AbstractSearch, JobSearch, DriverSearch

User = get_user_model()


class ExportCsvMixin:
    def export_as_csv(self, request, queryset):

        meta = self.model._meta
        field_names = [field.name for field in meta.fields]

        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename={}.csv'.format(meta)
        writer = csv.writer(response)

        writer.writerow(field_names)
        for obj in queryset:
            row = writer.writerow([getattr(obj, field) for field in field_names])

        return response

    export_as_csv.short_description = "Export Selected"


class HasOwnerFilter(admin.SimpleListFilter):
    title = 'has_owner'
    parameter_name = 'has_owner'

    def lookups(self, request, model_admin):
        return (
            ('Yes', 'Yes'),
            ('No', 'No'),
        )

    def queryset(self, request, queryset):
        value = self.value()
        if value == 'Yes':
            return queryset.exclude(created_by=None)
        elif value == 'No':
            return queryset.filter(created_by=None)
        return queryset


class FlowerTaxiAdminSite(admin.AdminSite):
    # changing titles and header of admin site
    site_header = "Flower Taxi"
    index_title = "Flower Taxi Administration"
    site_title = "Welcome To Flower Taxi Admin Portal"


class SearchAdmin(admin.ModelAdmin, ExportCsvMixin):
    list_display = (
        'id',
        'date_from',
        'date_to',
        'location',
        'price',
        'explanation',
        'creator_role',
        'quota',
        'taxi_detail',
        'created_by',
        'updated_by',
        'updated_at',
        'enable_site_notification',
        'gsm',
        'email',
    )


class ContractAdmin(admin.ModelAdmin, ExportCsvMixin):
    list_display = (
        'id',
        'job_search',
        'driver_search',
        'created_at',
        'updated_at',
        'created_by',
        'updated_by',
        'search_display'
    )

    def search_display(self, obj):
        return([
            search for search in JobSearch.objects.filter(
                Q(id=obj.manager_search.id)
                |
                Q(id=obj.driver_search.id)
            )])
    search_display.short_description = "Searches"


class DriverProfileAdmin(admin.ModelAdmin, ExportCsvMixin):
    list_display = ("license", "created_at_count", "created_by_count")

    # optimize query
    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        queryset = queryset.annotate(
            _created_at_count=Count("created_at", distinct=True),
            _created_by_count=Count("created_by", distinct=True),
        )
        return queryset

    # Calculated columns
    def created_at_count(self, obj):
        return obj._created_at_count

    def created_by_count(self, obj):
        return obj._created_by_count

    # order by specific column
    created_at_count.admin_order_field = '_created_at_count'
    created_by_count.admin_order_field = '_created_by_count'


class UserAdmin(admin.ModelAdmin, ExportCsvMixin):
    list_display = (
        'id',
        'username',
        'first_name',
        'last_name',
        'email',
        'date_joined',
        'last_login',
        'is_active'
    )


flower_admin_site = FlowerTaxiAdminSite(name='flower_taxi_admin')
# flower_admin_site.register(AbstractJobSearch, SearchAdmin)
flower_admin_site.register(User, UserAdmin)
flower_admin_site.register(Match, ContractAdmin)
