from django.db import models

from apps.webapp.mixins import AuditMixin
from apps.webapp.models import JobSearch, DriverSearch


class Match(AuditMixin):
    """
    to hold approved jobs
    which manager
    which driver
    contract date
    contract details
    with which search instance does it relate?
    ...
    """
    driver_search = models.ForeignKey(
        DriverSearch,
        on_delete=models.CASCADE,
        related_name='manager'
    )
    job_search = models.ForeignKey(
        JobSearch,
        on_delete=models.CASCADE,
        related_name='driver'
    )
    details = models.CharField(max_length=255, null=True, blank=True)
    rating = models.FloatField(null=True, blank=True)

    class Meta:
        unique_together = (('job_search', 'driver_search'), )
