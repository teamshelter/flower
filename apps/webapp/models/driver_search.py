from django.db import models

from apps.webapp.models import AbstractSearch


class DriverSearch(AbstractSearch):
    period = models.CharField(
        max_length=55,
        default='Günlük',
        choices=(
            ('Günlük', 'Günlük'),
            ('Devamlı', 'Devamlı'),
            ('Periyot', 'Periyot'),
        )
    )
    duration = models.CharField(max_length=30, null=True, blank=True)
    requested_fee = models.FloatField(null=True, blank=True)
