from django.db import models

from apps.webapp.mixins import TimeStampMixin
from apps.webapp.validators import validate_date_is_not_past, validate_phone_number


class AbstractSearch(TimeStampMixin):
    """
    abstract models special searches

    """
    quota = models.CharField(
        max_length=55,
        default='Farketmez',
        choices=(
            ('Sabahçı', 'Sabahçı'),
            ('Gececi', 'Gececi'),
            ('Farketmez', 'Farketmez'),
        )
    )
    exchange_location = models.CharField(max_length=255)
    primary_gsm = models.CharField(max_length=10, validators=[validate_phone_number])
    secondary_gsm = models.CharField(max_length=10, validators=[validate_phone_number], null=True, blank=True)
    fullname = models.CharField(db_column='name', max_length=60)
    age = models.IntegerField()
    is_retired = models.BooleanField(db_column='retired', default=False)
    comments = models.CharField(max_length=2048, null=True, blank=True)

    class Meta:
        abstract = True

    def __str__(self):
        return '%s %s' % (self.fullname, self.primary_gsm)

    @property
    def representation(self):
        return '%s, Kullanıcı: %s, %s bölgesinde, %s vardiyası için ilan oluşturdu.' % (
            str(self.id),
            self.fullname,
            self.exchange_location,
            str(self.quota)
        )
