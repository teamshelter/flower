from django.core.validators import RegexValidator
from django.db import models

from apps.webapp.models import AbstractSearch


class JobSearch(AbstractSearch):
    # TODO: validate
    card_no = models.CharField(
        max_length=255,
        validators=[
            RegexValidator(
                message='Girdiğiniz kart numarası yanlış formattadır.',
                regex='^[T](\d{9}[ ]\w{14})$'
            )
        ]
    )
    past_taxi_stop_list = models.CharField(max_length=1024, null=True, blank=True)
    reference = models.CharField(max_length=512, null=True, default=True)
    year_in_job = models.CharField(max_length=30, null=True, blank=True)
    current_employment = models.CharField(max_length=255, null=True, blank=True)
    is_married = models.BooleanField(db_column='married', default=False)
    blood_group = models.CharField(
        max_length=55,
        default='ARh(+)',
        choices=(
            ('ARh(+)', 'A Rh Pozitif'),
            ('ARh(-)', 'A Rh Negatif'),
            ('BRh(+)', 'B Rh Pozitif'),
            ('BRh(-)', 'B Rh Negatif'),
            ('ABRh(+)', 'AB Rh Pozitif'),
            ('ABRh(-)', 'AB Rh Negatif'),
            ('0Rh(+)', '0 Rh Pozitif'),
            ('0Rh(-)', '0 Rh Negatif'),
        )
    )
    year_in_istanbul = models.IntegerField(null=True, blank=True)
