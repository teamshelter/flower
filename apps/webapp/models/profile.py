from django.db import models
from django.db.models import Avg

from apps.webapp.mixins import TimeStampMixin
from apps.webapp.models import Match


class Profile(TimeStampMixin):
    username = models.CharField(max_length=255)

    @property
    def rating(self):
        driver_point = Match.objects.filter(
            driver_search__full_name=self.username
        ).aggregate(
            Avg("rating")
        ).get('rating__avg')

        manager_point = Match.objects.filter(
            manager_search__full_name=self.username
        ).aggregate(
            Avg("rating")
        ).get('rating__avg')

        if driver_point and manager_point:
            return (driver_point + manager_point) / 2

        if driver_point:
            return driver_point

        if manager_point:
            return manager_point

        return 0
