from django.contrib.auth.models import AnonymousUser
from django.db import transaction, IntegrityError
from django.utils.deprecation import MiddlewareMixin

from apps.logapp.models import AppLog


class LogAppMiddleware(MiddlewareMixin):

    def process_request(self, request):
        pass

    def process_response(self, request, response):
        reason = response.reason_phrase if response.reason_phrase else response.status_text
        status = '%s -%s-' % (response.status_code, reason)
        if request.path and request.method and request.get_host():
            log_instance = AppLog.objects.create(
                path=request.path,
                method=request.method,
                host=request.get_host(),
                status=status
            )
        else:
            log_instance = AppLog.objects.create(path=None, method=None, host=None)

        anonymous_user = request.user.__class__.__name__
        if not isinstance(request.user, AnonymousUser):
            log_instance.created_by = log_instance.created_by if log_instance.created_by else request.user
            log_instance.updated_by = request.user
        else:
            log_instance.created_by = log_instance.created_by if log_instance.created_by else anonymous_user
            log_instance.updated_by = anonymous_user
        log_instance.save()

        return response

