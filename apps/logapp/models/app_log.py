from django.db import models

from apps.webapp.mixins import AuditMixin


class AppLog(AuditMixin):
    path = models.CharField(max_length=2048, null=True)
    method = models.CharField(max_length=2048, null=True)
    status = models.CharField(max_length=4096, null=True)
    host = models.CharField(max_length=255, null=True)

