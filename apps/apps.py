import logging

from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class TaxiFlowerConfig(AppConfig):
    name = 'apps.webapp'
    label = 'taxi'
    verbose_name = _('Taxi Flower Web API')

    def ready(self):
        logging.info("Importing %s related services", self.verbose_name)
        try:
            import apps.webapp.receivers
        except ModuleNotFoundError as e:
            logging.error(e.__getattribute__("msg"))
        logging.info("Done!")


class LogAppConfig(AppConfig):
    name = 'apps.logapp'
    label = 'logapp'
    verbose_name = _('Taxi Flower Log App')

    def ready(self):
        logging.info("Importing %s related services", self.verbose_name)
        try:
            # TODO: burada uygulama için gerekli olan importlar var mıdiye kontrol etmek mantıklı olur mu
            import apps.webapp.receivers
        except ModuleNotFoundError as e:
            logging.error(e.__getattribute__("msg"))
        logging.info("Done!")
