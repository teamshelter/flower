# Dockerfile

# FROM directive instructing base image to build upon
FROM python:3.6-alpine

MAINTAINER Taxi Havuzu <mevlanaayas@gmail.com>

RUN apk update && apk add build-base postgresql-dev libffi-dev

#COPY requirements.txt /tmp/requirements.txt
#RUN pip install -r /tmp/requirements.txt
COPY requirements.txt /tmp/requirements.txt
RUN apk add --no-cache --virtual .build-deps \
  build-base postgresql-dev libffi-dev \
    && pip install -r /tmp/requirements.txt \
    && find /usr/local \
        \( -type d -a -name test -o -name tests \) \
        -o \( -type f -a -name '*.pyc' -o -name '*.pyo' \) \
        -exec rm -rf '{}' + \
    && runDeps="$( \
        scanelf --needed --nobanner --recursive /usr/local \
                | awk '{ gsub(/,/, "\nso:", $2); print "so:" $2 }' \
                | sort -u \
                | xargs -r apk info --installed \
                | sort -u \
    )" \
    && apk add --virtual .rundeps $runDeps \
    && apk del .build-deps

# Copy application
WORKDIR /apps/flower

COPY ./ ./

# COPY startup script into known file location in container
COPY start.sh /start.sh

# EXPOSE port 8000 to allow communication to/from server
EXPOSE 8000

# CMD specifcies the command to execute to start the server running.
CMD ["/start.sh"]
# done!